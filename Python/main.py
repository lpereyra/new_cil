import numpy as np
import lector as lee
import draw as graf
import matplotlib.pyplot as plt
import sys

if __name__ == '__main__':

  Path_cil = "../"
  Name_cil = "104_16917_matrix_LIN_0.79_0.17"
  Nfile_cil = 1
  if 'fixed' in Name_cil:
    fixed = True
  else: 
    fixed = False

  POSFACTOR = 1000.  

  ####### BINES LONGITUD #######
  print 'Ingresar Media'
  mean = [float(x) for x in raw_input().split(',')]
  mean.sort()

  par_nod, fil_nod = [], []
  Save_Seg ,Save_Pares = False, False
  ##############################################################

  num_bins = 5

  print 'Guardar Segmentos?'
  select = raw_input() 

  if('s' in select or 'S' in select or 'y' in select or 'Y' in select):

    Save_Seg = True

    flag, longitud, MNod, data = \
    lee.read_new(POSFACTOR, Path_cil, Name_cil, Nfile_cil, mean)

    print len(data)

    title = "SEG"
    if "sin_vel" in Name_cil: title += "_sin_vel"
    if "media" in Name_cil:   title += "_media"
    lista_pho, lista_vel = graf.realizo_graficos(num_bins, mean, data, MNod, longitud, flag, LOG=True, name_input=title)

    percent = np.arange(num_bins)+1

    title = "seg_q"
    if "sin_vel" in Name_cil: title += "_sin_vel"
    if "media" in Name_cil:   title += "_media"
    graf.save_pdf(lista_pho,lista_vel,title,num_bins,mean,percent,fixed)

    title = "seg_long"
    if "sin_vel" in Name_cil: title += "_sin_vel"
    if "media" in Name_cil:   title += "_media"
    graf.save_pdf(lista_pho,lista_vel,title,num_bins,mean,percent,fixed)

    del lista_pho,lista_vel
